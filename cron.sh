#!/bin/bash

set -eu

wp='sudo -u www-data -i -- /app/pkg/wp --path=/app/data/public/'

if $($wp core is-installed --network); then
    echo "=> Run multi-site cron job"
    site_urls=$($wp site list --fields=url --archived=0 --deleted=0 --format=csv | sed 1d)
    for site_url in ${site_urls}; do
        echo "==> Run cron job of ${site_url}"
        $wp cron event run --due-now --url="${site_url}"
    done
else
    echo "=> Run cron job"
    $wp cron event run --due-now
fi
