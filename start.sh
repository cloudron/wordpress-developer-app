#!/bin/bash

set -eu

wp='sudo -u www-data -i -- /app/pkg/wp --path=/app/data/public/ --skip-themes --skip-plugins'

configure_mailer_plugin() {
    echo "=> Configuring smtp-mailer plugin"
    # newer unmanaged WP uses smtp-mailer
    if $($wp core is-installed --network); then
        site_urls=$($wp site list --fields=url --archived=0 --deleted=0 --format=csv | sed 1d)
    else
        site_urls="${CLOUDRON_APP_ORIGIN}"
    fi

    for site_url in ${site_urls}; do
        echo "==> Configuring smtp-mailer plugin for ${site_url}"
        smtp_password=$(echo -n "${CLOUDRON_MAIL_SMTP_PASSWORD}" | base64 -w 0 -)

        # preserve any existing force_from_address
        force_from_address=""
        if mail_config=$($wp --format=json --url="${site_url}" option get smtp_mailer_options); then
            force_from_address=$(echo "${mail_config}" | jq -r .force_from_address) # is blank if missing from json
        fi

        mailConfig=$(cat <<EOF
        {
            "smtp_host"                 : "${CLOUDRON_MAIL_SMTP_SERVER}",
            "smtp_auth"                 : true,
            "smtp_username"             : "${CLOUDRON_MAIL_SMTP_USERNAME}",
            "smtp_password"             : "${smtp_password}",
            "type_of_encryption"        : "none",
            "smtp_port"                 : "${CLOUDRON_MAIL_SMTP_PORT}",
            "from_email"                : "${CLOUDRON_MAIL_FROM}",
            "from_name"                 : "${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-WordPress}",
            "force_from_address"        : "${force_from_address}",
            "disable_ssl_verification"  : ""
        }
EOF
        )
        $wp --format=json --url="${site_url}" option update smtp_mailer_options "${mailConfig}"
    done
}

mkdir -p /app/data/apache /run/wordpress/sessions

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

[[ ! -f /app/data/apache/mpm_prefork.conf ]] && cp /app/pkg/mpm_prefork.conf /app/data/apache/mpm_prefork.conf

if [[ ! -d "/app/data/public" ]]; then
    cp -rf /app/code/wordpress /app/data/public
    chown -R www-data:www-data /app/data

    echo "=> Create initial WordPress config"
    $wp config create --dbname="${CLOUDRON_MYSQL_DATABASE}" --dbuser="${CLOUDRON_MYSQL_USERNAME}" --dbpass="${CLOUDRON_MYSQL_PASSWORD}" --dbhost="${CLOUDRON_MYSQL_HOST}:${CLOUDRON_MYSQL_PORT}"

    echo "=> Install WordPress"
    # --skip-email is part of 0.23.0 https://github.com/wp-cli/wp-cli/pull/2345 and https://github.com/wp-cli/wp-cli/issues/1164
    $wp core install --skip-email --url="${CLOUDRON_APP_ORIGIN}" --title="My website" --admin_user="admin" --admin_password="changeme" --admin_email="${CLOUDRON_MAIL_FROM}"

    $wp config set --raw DISABLE_WP_CRON true

    # this sets up a random cookie name
    random=$(pwgen -1s 32)
    $wp config set --raw COOKIEHASH "md5('${random}')"

    $wp config set --raw WP_DEBUG false
    $wp config set --raw WP_DEBUG_LOG false
    $wp config set --raw WP_DEBUG_DISPLAY false
fi

echo "=> Changing permissions"
chown -R www-data:www-data /app/data /run/wordpress

# Update db settings first. otherwise, the domain/mail changes will overwrite the original db when cloning
echo "=> Updating db settings"
$wp config set DB_HOST "${CLOUDRON_MYSQL_HOST}:${CLOUDRON_MYSQL_PORT}"
$wp config set DB_NAME "${CLOUDRON_MYSQL_DATABASE}"
$wp config set DB_USER "${CLOUDRON_MYSQL_USERNAME}"
$wp config set DB_PASSWORD "${CLOUDRON_MYSQL_PASSWORD}"

echo "=> Updating redis settings"
if [[ -n "${CLOUDRON_REDIS_HOST:-}" ]]; then
    echo "==> Install redis plugin"

    # update the options first. WP redis installs a object-cache.php which is loaded even if --skip-plugins :/
    # after a restore, is-installed will fail with old redis config values.
    $wp config set WP_REDIS_HOST "${CLOUDRON_REDIS_HOST}"
    $wp config set WP_REDIS_PORT "${CLOUDRON_REDIS_PORT}"
    $wp config set WP_REDIS_PASSWORD "${CLOUDRON_REDIS_PASSWORD}"

    if ! $wp plugin is-installed redis-cache; then
        $wp plugin install /app/pkg/wp-redis-cache.zip
    fi

    $wp plugin activate redis-cache

    # this copies over the object-cache.php file into wp core. this require proper redis credentials
    # something enabling can fail if redis is not used with message to use wp redis update-dropin
    if ! sudo -u www-data -i -- /app/pkg/wp --path=/app/data/public/ --skip-themes redis enable; then
        echo "=> Unable to enable redis. Maybe another cache plugin is in use"
    fi
else
    rm -f /app/data/public/wp-content/object-cache.php
    $wp plugin deactivate redis-cache || true
    $wp plugin uninstall redis-cache || true
    $wp config delete WP_REDIS_HOST || true
    $wp config delete WP_REDIS_PORT || true
    $wp config delete WP_REDIS_PASSWORD || true
fi

# note that is-installed only works if db settings are up to date
if $wp core is-installed; then
    echo "=> Updating domain related settings"
    # This keeps the values in Settings -> Site/WordPress Address read-only
    $wp config set WP_HOME "${CLOUDRON_APP_ORIGIN}"
    $wp config set WP_SITEURL "${CLOUDRON_APP_ORIGIN}"

    # This is only done for keeping the db dumps more useful
    $wp option update siteurl "${CLOUDRON_APP_ORIGIN}"
    $wp option update home "${CLOUDRON_APP_ORIGIN}"
else
    echo "=> WordPress does not seem to be working or is not installed"
fi

if $($wp core is-installed --network); then
    readonly wp_option_get="$wp --format=json --quiet site option get"
    readonly wp_option_update="$wp --format=json --quiet site option update"
else
    readonly wp_option_get="$wp --format=json --quiet option get"
    readonly wp_option_update="$wp --format=json --quiet option update"
fi

if [[ -n "${CLOUDRON_MAIL_SMTP_SERVER:-}" ]]; then
    echo "=> Install smtp mail plugin"
    if ! $wp plugin is-installed smtp-mailer; then
        $wp plugin install /app/pkg/smtp-mailer.zip
    fi
    $wp plugin activate smtp-mailer

    configure_mailer_plugin
else
    echo "=> Removing mail configuration"
    if $($wp core is-installed --network); then
        site_urls=$($wp site list --fields=url --archived=0 --deleted=0 --format=csv | sed 1d)
    else
        site_urls="${CLOUDRON_APP_ORIGIN}"
    fi

    $wp plugin deactivate smtp-mailer || true
    $wp plugin uninstall smtp-mailer || true
    for site_url in ${site_urls}; do
        $wp --url="${site_url}" option delete smtp_mailer_options || true
    done
fi

echo "=> Creating credentials.txt"
sudo -u www-data -- sed -e "s,MYSQL_HOST,${CLOUDRON_MYSQL_HOST}," \
    -e "s,MYSQL_PORT,${CLOUDRON_MYSQL_PORT}," \
    -e "s,MYSQL_USERNAME,${CLOUDRON_MYSQL_USERNAME}," \
    -e "s,MYSQL_PASSWORD,${CLOUDRON_MYSQL_PASSWORD}," \
    -e "s,MYSQL_DATABASE,${CLOUDRON_MYSQL_DATABASE}," \
    -e "s,MYSQL_URL,${CLOUDRON_MYSQL_URL}," \
    -e "s,REDIS_SERVER,${CLOUDRON_REDIS_HOST:-NA}," \
    -e "s,REDIS_PORT,${CLOUDRON_REDIS_PORT:-NA}," \
    -e "s,REDIS_PASSWORD,${CLOUDRON_REDIS_PASSWORD:-NA}," \
    /app/pkg/credentials.template > /app/data/credentials.txt

# sendmail is optional
sudo -u www-data -- sed -e "s,MAIL_SMTP_SERVER,${CLOUDRON_MAIL_SMTP_SERVER:-NA}," \
    -e "s,MAIL_SMTP_PORT,${CLOUDRON_MAIL_SMTP_PORT:-NA}," \
    -e "s,MAIL_SMTPS_PORT,${CLOUDRON_MAIL_SMTPS_PORT:-NA}," \
    -e "s,MAIL_SMTP_USERNAME,${CLOUDRON_MAIL_SMTP_USERNAME:-NA}," \
    -e "s,MAIL_SMTP_PASSWORD,${CLOUDRON_MAIL_SMTP_PASSWORD:-NA}," \
    -e "s,MAIL_FROM,${CLOUDRON_MAIL_FROM:-NA}," \
    -e "s,MAIL_DOMAIN,${CLOUDRON_MAIL_DOMAIN:-NA}," \
    -i /app/data/credentials.txt

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    default_role=""
    if $wp plugin is-installed authldap; then
        default_role=$($wp --format=json option get authLDAPOptions | jq -r ".DefaultRole")
        echo "==> Uninstall LDAP plugin (authldap)"
        $wp plugin deactivate --uninstall authldap
    fi

    if ! $wp plugin is-installed openid-connect-generic; then
        echo "==> Install OIDC plugin"
        $wp plugin install /app/pkg/openid-connect-generic.zip
        mv /app/data/public/wp-content/plugins/openid-connect-generic-* /app/data/public/wp-content/plugins/openid-connect-generic
        $wp plugin activate openid-connect-generic

        $wp plugin install /app/pkg/cloudron-sso.zip
        $wp plugin activate cloudron-sso
    fi

    if oidcConfig=$(wp --format=json option get openid_connect_generic_settings); then
        # CLOUDRON_OIDC_PROVIDER_NAME is not supported
        oidcConfig=$(echo $oidcConfig \
                    | jq ".login_type=\"button\"" \
                    | jq ".client_id=\"${CLOUDRON_OIDC_CLIENT_ID}\"" \
                    | jq ".client_secret=\"${CLOUDRON_OIDC_CLIENT_SECRET}\"" \
                    | jq ".scope=\"openid profile email\"" \
                    | jq ".endpoint_login=\"${CLOUDRON_OIDC_AUTH_ENDPOINT}\"" \
                    | jq ".endpoint_userinfo=\"${CLOUDRON_OIDC_PROFILE_ENDPOINT}\"" \
                    | jq ".endpoint_token=\"${CLOUDRON_OIDC_TOKEN_ENDPOINT}\"" \
                    | jq ".endpoint_end_session=\"\"" \
                    | jq ".identity_key=\"preferred_username\"" \
                    | jq ".no_sslverify=\"0\"" \
                    | jq ".alternate_redirect_uri=\"0\"" \
                    | jq ".nickname_key=\"preferred_username\"" \
                    | jq ".email_format=\"{email}\"" \
                    | jq ".displayname_format=\"{given_name} {family_name}\"" \
                    | jq ".identify_with_username=\"1\"" \
                    | jq ".token_refresh_enable=\"0\"" \
                    | jq ".link_existing_users=\"1\"" \
                    | jq ".create_if_does_not_exist=\"1\"" \
                    | jq ".redirect_user_back=\"1\"" \
                    | jq ".redirect_on_logout=\"1\"" \
            )
    else
        oidcConfig=$(cat <<EOF
{
  "login_type": "button",
  "client_id": "${CLOUDRON_OIDC_CLIENT_ID}",
  "client_secret": "${CLOUDRON_OIDC_CLIENT_SECRET}",
  "scope": "openid profile email",
  "endpoint_login": "${CLOUDRON_OIDC_AUTH_ENDPOINT}",
  "endpoint_userinfo": "${CLOUDRON_OIDC_PROFILE_ENDPOINT}",
  "endpoint_token": "${CLOUDRON_OIDC_TOKEN_ENDPOINT}",
  "endpoint_end_session": "",
  "acr_values": "",
  "identity_key": "preferred_username",
  "no_sslverify": "0",
  "http_request_timeout": "5",
  "enforce_privacy": "0",
  "alternate_redirect_uri": "0",
  "nickname_key": "preferred_username",
  "email_format": "{email}",
  "displayname_format": "{given_name} {family_name}",
  "identify_with_username": "1",
  "state_time_limit": "180",
  "token_refresh_enable": "0",
  "link_existing_users": "1",
  "create_if_does_not_exist": "1",
  "redirect_user_back": "1",
  "redirect_on_logout": "1",
  "default_role": "${default_role:-editor}",
  "enable_logging": "0",
  "log_limit": "1000"
}
EOF
)
    fi

    echo "==> Configuring OIDC"
    $wp --format=json option update openid_connect_generic_settings "${oidcConfig}"
fi

echo "==> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
