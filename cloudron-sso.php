<?php

/**
 * Cloudron SSO
 *
 * This plugin provides default settings for Cloudron SSO
 *
 * @package   Cloudron SSO
 * @category  General
 * @author    Cloudron
 * @copyright 2024 Cloudron
 * @license   http://www.gnu.org/licenses/gpl-2.0.txt GPL-2.0+
 * @link      https://git.cloudron.io/cloudron/wordpress-managed-app
 *
 * @wordpress-plugin
 * Plugin Name:       Cloudron SSO
 * Plugin URI:        https://git.cloudron.io/cloudron/wordpress-managed-app
 * Description:       Cloudron SSO Settings
 * Version:           1.0.0
 * Requires at least: 5.0
 * Requires PHP:      7.4
 * Author:            Cloudron
 * Author URI:        http://cloudron.io
 * Text Domain:       cloudron-sso
 * Domain Path:       /languages
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Requires Plugins:  openid-connect-generic
 */

/**
 * openid-connect-generic plugin tweaks
 */

/**
 * Change login button text
 */
add_filter('openid-connect-generic-login-button-text', function( $text ) {
    $text = __('Login with Cloudron');
    
    return $text;
});

/**
 * Set a default role on OIDC user creating
 */
add_filter('openid-connect-generic-alter-user-data', function( $user_data, $user_claim ) {
    // Don't register any user with their real email address. Create a fake internal address.
    if ( empty( $user_data['role'] ) ) {
        $settings = get_option( 'openid_connect_generic_settings', [] );
        $user_data['role'] = isset( $settings['default_role'] ) && ! empty( $settings['default_role'] ) ? $settings['default_role'] : 'editor';
    }

    return $user_data;
}, 10, 2);

/**
 * Add default role select box on the settings page of openid-connect-generic plugin
 */
add_filter('openid-connect-generic-settings-fields', function( $fields ) {

    $editable_roles = wp_roles()->roles;
    $roles = [];
    foreach ($editable_roles as $role => $details) {
        $roles[ esc_attr( $role ) ] = translate_user_role( $details['name'] );
    }

    // A select field for default user role
    $fields['default_role'] = array(
        'title' => __( 'Default user role', 'cloudron-sso' ),
        'description' => __( 'A role set to OIDC users.', 'cloudron-sso' ),
        'type' => 'select',
        'options' => $roles,
        'section' => 'user_settings',
    );

    return $fields;
});
