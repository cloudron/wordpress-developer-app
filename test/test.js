#!/usr/bin/env node

/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    let browser, app, mediaLink;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    async function login(username, password) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/wp-login.php');
        await browser.sleep(2000); // there seems to be some javascript that gives auto-focus to username
        await browser.findElement(By.id('user_login')).sendKeys(username);
        await browser.findElement(By.id('user_pass')).sendKeys(password);
        await browser.findElement(By.xpath('//input[@value="Log In"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//h1[text()="Dashboard"]')), TIMEOUT);
    }

    async function loginOIDC(username, password, alreadyAuthenticated=true) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/wp-login.php');
        await browser.sleep(2000); // there seems to be some javascript that gives auto-focus to username

        await waitForElement(By.xpath('//div[@class="openid-connect-login-button"]'));
        await browser.findElement(By.xpath('//div[@class="openid-connect-login-button"]/a')).click();

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await waitForElement(By.xpath('//h1[text()="Dashboard"]'));
    }

    async function logout() {
        await browser.manage().deleteAllCookies();
        await browser.executeScript('localStorage.clear();');
        await browser.executeScript('sessionStorage.clear();');
    }

    async function checkNonAdminDashboard() {
        await browser.get('https://' + app.fqdn + '/wp-admin/');
        await browser.sleep(3000);
        await browser.wait(until.elementLocated(By.xpath('//div[@class="wp-menu-name" and text()="Profile"]')), TIMEOUT);
    }

    async function checkCredentials() {
        const out = execSync(`cloudron exec --app ${app.id} -- cat /app/data/credentials.txt || true`, { encoding: 'utf8' });
        expect(out.toString('utf8').indexOf('MySQL Credentials')).to.not.be(-1);
    }

    async function checkPost() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.xpath('//h2/a[text()="Hello Cloudron!"]')), TIMEOUT);
    }

    async function editPost() {
        await browser.get('https://' + app.fqdn + '/wp-admin/post.php?post=1&action=edit');
        await browser.wait(until.elementLocated(By.xpath('//div[contains(@class, "edit-post-welcome-guide")]//button[contains(@aria-label,"Close")]')), TIMEOUT);
        await browser.findElement(By.xpath('//div[contains(@class, "edit-post-welcome-guide")]//button[contains(@aria-label,"Close")]')).click();
        await browser.sleep(3000);
        await browser.switchTo().frame(browser.findElement(By.name('editor-canvas')));
        await browser.findElement(By.xpath('//h1[contains(@class, "wp-block-post-title")]')).sendKeys(Key.chord(Key.CONTROL, 'a') + Key.chord(Key.COMMAND, 'a') + Key.BACK_SPACE);
        await browser.findElement(By.xpath('//h1[contains(@class, "wp-block-post-title")]')).sendKeys('Hello Cloudron!');
        await browser.sleep(3000); // Update button will become enabled
        await browser.switchTo().defaultContent();
        if (app.manifest.version === '3.5.5') {
            await browser.findElement(By.xpath('//button[contains(text(), "Update")]')).click();
        } else {
            await browser.findElement(By.xpath('//button[contains(text(), "Save")]')).click();
        }
        await browser.wait(until.elementLocated(By.xpath('//*[contains(text(), "Post updated.")]')), TIMEOUT);
        await browser.sleep(3000);
    }

    async function uploadMedia() {
        await browser.get('https://' + app.fqdn + '/wp-admin/media-new.php?browser-uploader');
        await browser.wait(until.elementLocated(By.id('async-upload')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@id="async-upload" and @type="file"]')).sendKeys(path.resolve(__dirname, '../logo.png'));
        await browser.findElement(By.id('html-upload')).click();
        await browser.wait(async function () {
            const url = await browser.getCurrentUrl();
            return url === 'https://' + app.fqdn + '/wp-admin/upload.php';
        }, TIMEOUT);
    }

    async function checkMedia(item) {
        await browser.get(`https://${app.fqdn}/wp-admin/upload.php?item=${item}`); // there's got to be a better way..
        await browser.wait(until.elementLocated(By.xpath('//*[text()="Attachment details"]')), TIMEOUT);
        const srcLink = await browser.findElement(By.xpath('//img[@class="details-image"]')).getAttribute('src');
        console.log('media is located at ', srcLink);
        mediaLink = srcLink;
    }

    async function checkMediaLink() {
        const response = await superagent.get(mediaLink);
        expect(response.status).to.be(200);
    }

    async function checkSendEmail() {
        await browser.get(`https://${app.fqdn}/wp-admin/options-general.php?page=smtp-mailer-settings&action=test-email`);
        await browser.wait(until.elementLocated(By.id('smtp_mailer_to_email')), TIMEOUT);
        await browser.findElement(By.id('smtp_mailer_to_email')).sendKeys('test@cloudron.io');
        await browser.findElement(By.id('smtp_mailer_email_subject')).sendKeys('test subject');
        await browser.findElement(By.id('smtp_mailer_email_body')).sendKeys('too much starch');
        await browser.findElement(By.id('smtp_mailer_send_test_email')).click();
        await browser.wait(until.elementLocated(By.xpath('//*[text()[contains(., "250 Message Queued")]]')), TIMEOUT); // https://stackoverflow.com/questions/42778006/selenium-xpath-searching-for-element-by-innerhtml
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can get the main page', async function () {
        const response = await superagent.get('https://' + app.fqdn);
        expect(response.status).to.eql(200);
    });

    it('can admin login', login.bind(null, 'admin', 'changeme'));

    it('is an admin dashboard', async function () {
        await browser.wait(until.elementLocated(By.xpath('//div[@class="wp-menu-name" and contains(text(), "Plugins")]')), TIMEOUT);
    });

    it('can send email', checkSendEmail);
    it('can edit', editPost);
    it('can upload media', uploadMedia);
    it('can see media', checkMedia.bind(null, 8));
    it('can see media link', checkMediaLink);
    it('can see credentials', checkCredentials);
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password, false));
    it('is non-admin dashboard', checkNonAdminDashboard);
    it('can logout', logout);

    it('can restart app', function (done) {
        execSync('cloudron restart --app ' + app.id);
        done();
    });

    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can send email', checkSendEmail);
    it('can see updated post', checkPost);
    it('can see media link', checkMediaLink);
    it('can see credentials', checkCredentials);
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('is non-admin dashboard', checkNonAdminDashboard);
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can see updated post', checkPost);
    it('can see media link', checkMediaLink);
    it('can see credentials', checkCredentials);
    it('can send email', checkSendEmail);
    it('can admin login', login.bind(null, 'admin', 'changeme'));

    it('runs cron jobs', function (done) {
        this.timeout(6 * 60 * 1000); // cron runs only every 5 minutes
        console.log('It can take upto 6 mins to detect that cron is working');

        function checkLogs() {
            const logs = execSync('cloudron logs --lines 1000 --app ' + app.id).toString('utf8');
            if (logs.indexOf('Success: Executed a total of') !== -1) { console.log(); return done(); }

            process.stdout.write('.');
            setTimeout(checkLogs, 45000);
        }

        setTimeout(checkLogs, 45000);
    });

    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('is non-admin dashboard', checkNonAdminDashboard);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.manage().deleteAllCookies();
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        getAppInfo();
        mediaLink = mediaLink.replace(LOCATION, LOCATION + '2');
    });

    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can send email', checkSendEmail);
    it('can see updated post', checkPost);
    it('can see media link', checkMediaLink);
    it('can see credentials', checkCredentials);
    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('is non-admin dashboard', checkNonAdminDashboard);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // No SSO
    it('install app (no sso)', function () {
        execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can login (no sso)', login.bind(null, 'admin', 'changeme'));

    it('is an admin dashboard (no sso)', function (done) {
        browser.wait(until.elementLocated(By.xpath('//div[@class="wp-menu-name" and contains(text(), "Plugins")]')), TIMEOUT).then(function () { done(); });
    });

    it('can logout', logout);

    it('uninstall app (no sso)', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app - update', function () {
        execSync('cloudron install --appstore-id org.wordpress.unmanaged.cloudronapp --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
    });
    it('can admin login', loginOIDC.bind(null, 'admin', 'changeme'));
    it('can edit', editPost);
    it('can upload media', uploadMedia);

    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, EXEC_ARGS);
    });

    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can see updated post', checkPost);
    it('can see media', checkMedia.bind(null, 7));
    it('can see media link', checkMediaLink);
    it('can see credentials', checkCredentials);
    it('can send email', checkSendEmail);
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('is non-admin dashboard', checkNonAdminDashboard);
    it('can logout', logout);

    it('uninstall app', function (done) {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        done();
    });
});

